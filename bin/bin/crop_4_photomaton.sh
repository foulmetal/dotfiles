#!/bin/bash

if [ $# -ne 1 ]; then
	echo "wrong parameter's number"
	exit 1
fi

filename="$1"

filename_pref=${filename%.*}
filename_suf=${filename#*.}


crop_dim_x=489
crop_dim_y=644

init_y=14
new_y=$init_y

for i in 1 2 3 4; do
	geometry="${crop_dim_x}x${crop_dim_y}+0+${new_y}"
	convert -crop "$geometry" "$filename" "${filename_pref}-$i.${filename_suf}"
	new_y=$((new_y+crop_dim_y+1))
done
