# Dotfiles (public)

## Introduction

This project is related to config files that are stored in the home directory on an UNIX-like system : Linux, \*BSD, Darwin (OS X).
These files are generally hidden and preceding with a dot (``.``), this is why we call them **dotfiles**.
I also put my script files (stored in $HOME/bin), those are not dotfiles but those text files are relatively small and similarly usefull.

This project doesn't record any private information like credentials, people's name, bookmarks, etc.

It consists on severals folder (top folder). Each folder represents an app name that stored its config files and the tree structure is preserved.

This project is used for keep a track on my config files and also to centralized them to a unique entry point.

## How to use?

This git project supposed to be cloned into the ``$HOME`` base directory (e.g. into ``~/.dotfiles/``)

```
$ git clone https://framagit.org/foulmetal/dotfiles.git .dotfiles
```

In order to use those configs, they have to be [symlinked](https://en.wikipedia.org/wiki/Symbolic_link) to your ``$HOME`` base directory.

You can use the [``stow``](https://www.gnu.org/software/stow/) tool in order to achieve this per config app folder.
Example:
```
$ cd ~/.dotfiles
$ stow -t ~ tmux
```
``-t`` switch is not necessary as long as the project folder is located directly to $HOME
