#!/bin/bash

cmd="$1"
shift
escapeLF() { sed ':a;N;$!ba;s/\n/\\n/g'; }

_notify() {
	urgency="$1"
	msg="$2"
	notify-send -t 1000 -u "$urgency" "$cmd" "$msg"
}

{ eval "$cmd" "$@" 2>&3 | while read; do _notify normal "$REPLY";done; } 3>&1 1>&2 |
	escapeLF | { read && _notify critical "$REPLY"; }
