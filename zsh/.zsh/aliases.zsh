# this file is used by Zsh and also used by the –/bin/dmenu_recent_aliases.sh
alias squirrel-sql='(cd ~/Application/Squirrel; java -jar squirrel-sql.jar)'
alias ffx-priv='firefox --private-window'
alias c="${_pygmentize_cmd}"
alias grepHtmlHeader="perl -ne 'if (\$p) { print; \$p = 0 }; if (/^\* Connection/) { print; \$p++ }  print if /^\* Connected|^< Age|^< HTTP/'"
#alias dotenv="set -a; [ -f \$1 ] && . ./\$1 && echo \"\$1 exported!\"; shift; set +a"
#alias dotenv='export $(grep -v "^#" $1 | xargs -d "\n")'
alias ssm='aws ssm get-parameters-by-path --with-decryption --recursive --path'
alias dkc='docker-compose'

alias earcon="bluetoothctl <<< 'connect A8:4D:4A:08:7A:C9' &> /dev/null"
alias eardiscon="bluetoothctl <<< 'disconnect A8:4D:4A:08:7A:C9' &> /dev/null"

function dotenv () {
	[ -f "$1" ] && source <(sed -E -n 's/[^#]+/export &/ p' $1) && echo "$1 exported!"
}
function tree () {
	find "$@" | sed -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'
}
