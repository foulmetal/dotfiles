#xscreensaver &
export PATH=$PATH:$HOME/bin
export _JAVA_AWT_WM_NONREPARENTING=1
export JAVA_HOME='/usr/lib/jvm/java-8-openjdk-amd64'

source ~/.xprofile

# Keyboard configuration
setxkbmap fr bepo
#setxkbmap -option caps:swapescape

## TouchPad configuration
# Natural Scrolling Enabled
xinput set-prop 12 307 1
xinput set-prop 23 307 1
#synclient HorizTwoFingerScroll=1 TapButton3=2 TouchpadOff=2
syndaemon -i 1 -d -t -K

## X App configuration
urxvtd -q -o -f
xrdb ~/.Xresources
#conky | while read line; do xsetroot -name "$line"; done & 

( sleep 2; nm-applet; ) &

# put a delay before screen settings
sleep 2

## screen configuration ##
sh ./my_session.d/screen-config.sh

xsetroot -solid black

exec i3
