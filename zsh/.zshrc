export EDITOR=vim
export TERMINAL=urxvtcd

_pygmentize_cmd='pygmentize -f 256 -O style=monokai -g'
export LESS='-R'
export LESSOPEN="|${_pygmentize_cmd} %s"

# If you come from bash you might have to change your $PATH.
export PATH=$HOME/.local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

fpath=(
	~/.zfunc
	${fpath[@]}
)

# all zsh files
typeset -U config_files
config_files=(~/.zsh/**/*.zsh)

for file in ${config_files}
do
  source $file
done

setopt complete_aliases

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"
#ZSH_THEME="schminitz"


# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git git-flow)

source $ZSH/oh-my-zsh.sh

mongorest() { mongorestore --drop --nsInclude "export.*" --nsFrom "export.*" --nsTo "ws.*" --archive="$1" }

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# tabtab source for jhipster package
# uninstall by removing these lines or running `tabtab uninstall jhipster`
# [[ -f /home/gsarradin/.config/yarn/global/node_modules/generator-jhipster/node_modules/tabtab/.completions/jhipster.zsh ]] && . /home/gsarradin/.config/yarn/global/node_modules/generator-jhipster/node_modules/tabtab/.completions/jhipster.zsh

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
alias grunner="docker run --rm -it -v $(pwd):$PWD -v /var/run/docker.sock:/var/run/docker.sock:rw -w $PWD/ --entrypoint="gitlab-runner" gitlab/gitlab-runner:alpine exec docker --docker-privileged"

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /home/dev/work/popup/services/as-product-price/node_modules/tabtab/.completions/serverless.zsh ]] && . /home/dev/work/popup/services/as-product-price/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /home/dev/work/popup/services/as-product-price/node_modules/tabtab/.completions/sls.zsh ]] && . /home/dev/work/popup/services/as-product-price/node_modules/tabtab/.completions/sls.zsh
# tabtab source for slss package
# uninstall by removing these lines or running `tabtab uninstall slss`
[[ -f /home/dev/work/popup/services/as-product-to-algolia/node_modules/tabtab/.completions/slss.zsh ]] && . /home/dev/work/popup/services/as-product-to-algolia/node_modules/tabtab/.completions/slss.zsh
