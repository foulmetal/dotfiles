#!/bin/sh

SCROT_DIR="$HOME/Images/scrots/"

if [ ! -d $SCROT_DIR ]; then
	echo "$SCROT_DIR doesn't exist or isn't a directory" 2>&1
	exit 1
fi

base_cmd='scrot'

if [ "$1" = "-s" ]; then
	base_cmd="$base_cmd -s";
	echo 'Select a window or area'
fi

scrot_file=$($base_cmd -e 'mv $f '"$SCROT_DIR"'; echo $f')
if [ $? -ne 0 ]; then
	echo "cmd failed" >&2
	exit 1;
fi

killall xclip
echo  "<img src='data:image/png;base64,"$(base64 -w0 $SCROT_DIR/$scrot_file)"' />" | ( xclip  -t text/html -selection clipboard &&  echo 'Screenshot pasted to clipboard'; )
