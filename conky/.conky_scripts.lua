function conky_battery()
	battery_short = conky_parse("${battery_short}")

	status = battery_short:sub(1, 1)

	status_translate = {
		C = "⚡CHR",
		D = "🔋 BAT",
		F = "☻ FULL",
		N = "X",
		E = "EMPTY",
		U = "? UNK"
	}

	return status_translate[status] .. battery_short:sub(2)
end


