## screen configuration ##

# conf: EliteBook 850 G5 (Core i7 vPro 8th) on Ubuntu 18.04
# laptop screen: eDP-1
# HDMI screen: HDMI-1 (left)
# USB-C screen: DP-2 (right)
echo 'OFF'
xrandr --output $externalScreenLeft --off
xrandr --output $externalScreenRight --off

echo 'ON'
xrandr --output $externalScreenLeft --mode 1920x1080
xrandr --output $externalScreenRight --mode 1920x1080

echo 'Dispositions'
# @Office: laptop below left screen and right screen
laptopScreen='eDP1'
externalScreenLeft='HDMI1'
externalScreenRight='DP2'

# @Office2: Romain place
laptopScreen='eDP1'
externalScreenRight='HDMI1'
externalScreenLeft='DP2'

xrandr --output $externalScreenLeft --auto --above $laptopScreen
xrandr --output $externalScreenRight --auto --above $laptopScreen
xrandr --output $externalScreenRight --auto --right-of $externalScreenLeft


# @Home: laptop right of left screen
laptopScreen='eDP1'
externalScreenLeft='HDMI1'
externalScreenRight='DP2'
#xrandr --output $externalScreenLeft --auto --left-of $laptopScreen


# Blue filter
echo 'Apply Blue filter'
for screen in $laptopScreen $externalScreenLeft $externalScreenRight; do
	xrandr --output $screen --gamma 1:0.82854786:0.64816570 --brightness 1
done

xsetroot -solid black

